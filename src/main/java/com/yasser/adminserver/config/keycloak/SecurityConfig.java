package com.yasser.adminserver.config.keycloak;

import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.header.writers.DelegatingRequestMatcherHeaderWriter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@KeycloakConfiguration
public class SecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

    @Autowired
    public void configureGlobal(
            AuthenticationManagerBuilder auth) throws Exception {

        KeycloakAuthenticationProvider keycloakAuthenticationProvider
                = keycloakAuthenticationProvider();
        keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(
                new SimpleAuthorityMapper());
        auth.authenticationProvider(keycloakAuthenticationProvider);
    }

    @Bean
    @Override
    protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
        return new RegisterSessionAuthenticationStrategy(
                new SessionRegistryImpl());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.csrf().disable();
        http.headers().frameOptions().sameOrigin();
        http.authorizeRequests()
                .antMatchers("/**")
                .authenticated()
                .and()
                .logout()
                .logoutUrl("/logout")
                //                .invalidateHttpSession(true)
                //                .deleteCookies("JSESSIONID")
                //        .logout()
                //                .logoutUrl("/j_spring_security_logout")
                .logoutSuccessUrl(
                        "http://localhost:9080/auth/realms/yasser/protocol/openid-connect/auth?response_type=code&client_id=spring-boot-jsf-app&redirect_uri=http%3A%2F%2Flocalhost%3A2222%2Fsso%2Flogin&state=0badab44-670e-4d77-a050-6cca2509808d&login=true&scope=openid");
        //        http://:9080/auth/realms/master/protocol/openid-connect/auth?client_id=security-admin-console&redirect_uri=http%3A%2F%2Flocalhost%3A9080%2Fauth%2Fadmin%2Fmaster%2Fconsole%2F&state=289cc76f-7303-4991-87fc-405ff48fb0d0&response_mode=fragment&response_type=code&scope=openid&nonce=e154d7a9-5331-4bbd-96ac-566c231a739d

        http.headers()
                .addHeaderWriter(new DelegatingRequestMatcherHeaderWriter(new AntPathRequestMatcher("/javax.faces.resource/**"),
                        (request, response) -> response.addHeader("Cache-Control", "private, max-age=864000")))
                .defaultsDisabled();
    }
}