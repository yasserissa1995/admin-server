package com.yasser.adminserver.config.keycloak;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class KeycloakController {

    /**
     * Propagates the logout to the Keycloak infrastructure
     * @param request
     * @return
     * @throws Exception
     */
    @PostMapping("/admin/logout")
    public String logout(HttpServletRequest request) throws Exception {
        request.logout();
        return "redirect:/admin";
    }

}
